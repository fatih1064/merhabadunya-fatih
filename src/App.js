import React, { Component } from 'react';
import './App.css';

class Dot extends Component {
  componentWillMount(){
    this.setItem();
    setTimeout(() => {this.setItem()}, 50);
    let intervalId = setInterval(() => {this.setItem()}, 10000);
    this.setState({intervalId:intervalId});
  }

  setItem(){
    const x = (Math.random() * 1000)/10;
    const y = (Math.random() * 1000)/10;
    const fontSize = (Math.random() * 16) + 15;
    const opacity = (Math.random() * 100) / 100;

    const item = <span style={{opacity:opacity, position:"fixed", fontSize:fontSize, top:`${y}%`, left:`${x}%`, textShadow:`0px 0px ${fontSize * 3}px rgba(255,255,255, ${opacity})`}}>.</span>;

    this.setState({
      item:item
    });
  }

  componentWillUnmount(){
    clearInterval(this.state.intervalId);
  }

  render(){
    return this.state.item;
  }
}

class Background extends Component {
  constructor(){
    super();
    this.state = { animate:false };
  }
  componentWillMount(){
    this.setDots();
  }
  setDots(){
    const dotLength = 40;
    const items = [];

    for(let i = 0; i <= dotLength; i++){
      items.push(<Dot key={i} />);
    }

    this.setState({
      items:items
    });
  }
  toggleAnim(e){
    this.setState({animate:e.target.checked});
  }
  render(){
    return (
      <div className="bg">
        <div className="toggle-anim">
          <h2 className="anim-title" onClick={() => this.setState({animate:!this.state.animate})}>Animasyonu {this.state.animate ? "kapatmak" : "açmak"} için tıkla</h2>
          <label className="switch-label">
            <input type="checkbox" onChange={(e) => this.toggleAnim(e)} checked={this.state.animate}/>
            <span className="cb-slider round"></span>
          </label>
        </div>
        {this.state.animate ? this.state.items : null}
      </div>
    );
  }
}

const Element = (props) => {
  const {element, color} = props;
  return (
    <div className="element" style={{backgroundColor:color}}>
      <h1 className="element-simge">
        <span className="asil-harf">{element.mainLetter}</span>
        <span className="diger-harfler">{element.otherLetters}</span>
      </h1>
      <span className="element-numara">
        {element.number}
      </span>
    </div>
  );
};

const Info = () => {
  return (
    <div className="app-info">
      <p>Hocam "Merhaba Dünya" artık baydı ondan bunu yaptım :D</p>
      <a className="link-default" href="https://bitbucket.org/fatih1064/merhabadunya-fatih/src" target="_blank" rel="noopener noreferrer">Kodlar</a>
    </div>
  );
};

class App extends Component {
  constructor(){
    super();
    this.state = {
      colors:[
        "#673AB7", "#00796B", "#4CAF50", "#8BC34A", "#AFB42B", "#FF9800", "#FFA000",
        "#F57C00", "#795548", "#0288D1", "#00BCD4", "#26A69A", "#66BB6A", "#43A047",
        "#7CB342", "#F44336", "#E53935", "#C62828", "#E91E63", "#C2185B", "#7B1FA2",
        "#3F51B5", "#333", "#141421", "#192024", "#33691E", "#E65100", "#212121", "#424242",
        "#616161", "#6D4C41", "#4E342E", "#795548", "#FF5722", "#FF6E40", "#BF360C", "#DD2C00"
      ],
      mElements:[
        { mainLetter:"M", otherLetters:"g", number:72 },
        { mainLetter:"M", otherLetters:"n", number:25 },
        { mainLetter:"M", otherLetters:"t", number:109 },
        { mainLetter:"M", otherLetters:"d", number:101 },
        { mainLetter:"M", otherLetters:"o", number:42 },
        { mainLetter:"M", otherLetters:"c", number:115 }
      ],
      eElements:[
        { mainLetter:"E", otherLetters:"s", number:99 },
        { mainLetter:"E", otherLetters:"r", number:68 },
        { mainLetter:"E", otherLetters:"u", number:63 }
      ],
      rElements:[
        { mainLetter:"R", otherLetters:"a", number:88 },
        { mainLetter:"R", otherLetters:"n", number:86 },
        { mainLetter:"R", otherLetters:"e", number:75 },
        { mainLetter:"R", otherLetters:"h", number:45 },
        { mainLetter:"R", otherLetters:"g", number:111 },
        { mainLetter:"R", otherLetters:"b", number:37 },
        { mainLetter:"R", otherLetters:"u", number:44 },
        { mainLetter:"R", otherLetters:"f", number:104 }
      ],
      hElements:[
        { mainLetter:"H", otherLetters:"f", number:72 },
        { mainLetter:"H", otherLetters:"s", number:108 },
        { mainLetter:"H", otherLetters:"e", number:2 },
        { mainLetter:"H", otherLetters:"o", number:67 },
        { mainLetter:"H", otherLetters:"", number:1 }
      ],
      aElements:[
        { mainLetter:"A", otherLetters:"c", number:89 },
        { mainLetter:"A", otherLetters:"l", number:13 },
        { mainLetter:"A", otherLetters:"m", number:95 },
        { mainLetter:"A", otherLetters:"r", number:18 },
        { mainLetter:"A", otherLetters:"s", number:33 },
        { mainLetter:"A", otherLetters:"t", number:85 }
      ],
      bElements:[
        { mainLetter:"B", otherLetters:"a", number:56 },
        { mainLetter:"B", otherLetters:"k", number:97 },
        { mainLetter:"B", otherLetters:"e", number:4 },
        { mainLetter:"B", otherLetters:"i", number:83 },
        { mainLetter:"B", otherLetters:"h", number:107 },
        { mainLetter:"B", otherLetters:"", number:5 },
        { mainLetter:"B", otherLetters:"r", number:35 }
      ],
      kElements:[
        { mainLetter:"K", otherLetters:"r", number:36 },
        { mainLetter:"K", otherLetters:"", number:19 }
      ],
      iElements:[
        { mainLetter:"I", otherLetters:"n", number:49 },
        { mainLetter:"I", otherLetters:"", number:53 },
        { mainLetter:"I", otherLetters:"r", number:77 }
      ],
      yElements:[
        { mainLetter:"Y", otherLetters:"b", number:70 },
        { mainLetter:"Y", otherLetters:"n", number:39 }
      ]
    }
  }
  componentWillMount(){
    this.setElements();
    setInterval(() => {this.setElements()}, 2000);
  }
  getRandomOpacity(){
    const letters = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"];
    const length = letters.length;
    const opac1index = Math.floor(Math.random() * (length));
    const opac2index = Math.floor(Math.random() * (length));

    const opacity = letters[opac1index] + letters[opac2index];
    return opacity;
  }
  getRandomColor(){
    const colors = this.state.colors;
    const length = colors.length;
    const colIndex = Math.floor(Math.random() * (length));

    return colors[colIndex] + this.getRandomOpacity();
  }
  getRandomEl(elements){
    const length = elements.length;
    const elIndex = Math.floor(Math.random() * (length));
    const color = this.getRandomColor();
    return {info:elements[elIndex], color:color};
  }
  setElements(){
    const firstWord = [this.getRandomEl(this.state.mElements), this.getRandomEl(this.state.eElements), this.getRandomEl(this.state.rElements), this.getRandomEl(this.state.hElements), this.getRandomEl(this.state.aElements), this.getRandomEl(this.state.bElements), this.getRandomEl(this.state.aElements)];

    const secondWord = [this.getRandomEl(this.state.kElements), this.getRandomEl(this.state.iElements), this.getRandomEl(this.state.mElements), this.getRandomEl(this.state.yElements), this.getRandomEl(this.state.aElements)];

    this.setState({
      firstWord:firstWord,
      secondWord:secondWord
    });
  }

  renderFirstWord(){
    const firstWord = this.state.firstWord;

    return firstWord.map((element, key) => {
      return <Element key={key} element={element.info} color={element.color} />
    });
  }

  renderSecondWord(){
    const secondWord = this.state.secondWord;

    return secondWord.map((element, key) => {
      return <Element key={key+7} element={element.info} color={element.color} />
    });
  }

  render() {
    return (
      <div className="app-merhaba-kimya">
        <Background />
        <div className="kelime">
          {this.renderFirstWord()}
        </div>
        <div className="kelime">
          {this.renderSecondWord()}
        </div>
        <Info />
      </div>
    );
  }
}

export default App;
